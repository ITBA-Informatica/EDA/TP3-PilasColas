# TP 3 - Pilas y Colas

# Consigna

1. Escribir un programa que lea expresiones matemáticas en notación posfija de la entrada estándar, e
imprima el resultado de evaluar dichas expresiones. Las operaciones que se deben soportar son
suma, resta, multiplicación y división.
2. Implementar un algoritmo que convierta una expresión matemática en notación infija a notación
posfija. Modificar el programa del ejercicio anterior para que evalúe expresiones en notación
infija. Utilizar para los operadores la misma precedencia que tienen en el lenguaje Java, y permitir
además el uso de paréntesis para modificar esta precedencia.
Ejemplo: La expresión “2 * (3 + 4) – 1” sería convertida a “2 3 4 + * 1 –” y finalmente evaluada
como 13.

3. Una cola de prioridades es una cola en donde los elementos tienen asignada una prioridad. Al
momento de encolar un elemento, se especifica su prioridad, y se lo agrega por delante de aquellos
que tengan menor prioridad qué él y detrás del último elemento de su misma prioridad.
Implementar la interfaz PriorityQueue, sin utilizar clases de la API de Java, teniendo en cuenta
los siguientes requerimientos de eficiencia:
    -  El método enqueue debe tener complejidad temporal O(N), siendo N la cantidad de
    prioridades en uso en ese instante.
    -  El método dequeue debe tener complejidad temporal O(1).

    ```java
    
    public interface PriorityQueue<T> {
    /**
    * Encola un elemento.
    * @param priority Un número mayor a cero que especifica la prioridad. Cuanto
    * menor es este número mayor es la prioridad.
    */
    public void enqueue(T elem, int priority);
    /**
    * Desencola un elemento.
    * @throws NoSuchElementException Si la cola está vacía.
    */
    public T dequeue();
    /**
    * Evalúa si la cola está vacía o no.
    */
    public boolean isEmpty();
    }
    ```

4. -  Resolver el ejercicio 15 del TP 1 de manera iterativa, utilizando explícitamente un stack.

5. Escribir un programa que permita simular la performance de un servidor de impresión.
    -  El servidor recibe solicitudes de impresión desde distintas terminales, y las procesa en el
    orden en el que las recibe. Si se encuentra ocupado, los trabajos son encolados.
    -  La probabilidad de que en una unidad de tiempo arribe un trabajo al servidor está dada por
    el parámetro arrivalProbability.
    - El tiempo que tarda el servidor en procesar un trabajo está uniformemente distribuido entre
    los parámetros minServiceTime y maxServiceTime.
    -  El tiempo total de la simulación está dado por el parámetro simulationTime.
    
    En función de estos parámetros, el programa debe imprimir como resultado la cantidad total de
    trabajos atendidos, el tiempo promedio de espera y la cantidad promedio de trabajos en cola.

6. - Escribir un programa que permita simular las colas que se forman en las cajas de un supermercado.
    - El supermercado cuenta con N cajas operando simultáneamente. Los clientes forman fila a
    la espera de ser atendidos.
    - La probabilidad de que en una unidad de tiempo arribe un cliente a las cajas está dada por
    el parámetro arrivalProbability. El cliente se ubicará en la caja que menor cantidad de
    gente tenga en la cola.
    - El tiempo que tarda un cajero en atender a un cliente está uniformemente distribuido entre
    los parámetros minServiceTime y maxServiceTime.
    - El tiempo total de la simulación está dado por el parámetro simulationTime.
    En función de estos parámetros, el programa debe imprimir por consola la longitud de cada cola en
    cada instante de la simulación, y al terminar debe reportar el tiempo mínimo, máximo y promedio
    de espera de los clientes, el promedio de longitud de las colas y el tiempo promedio ocioso de los
    cajeros.
7. Implementar una cola que internamente almacene los elementos en stacks. Las operaciones
enqueue y dequeue deben definirse en función de las operaciones push y pop de un stack.
Determinar la cantidad mínima de stacks requeridos.
8. El sistema de edición de texto LaTeX utiliza el concepto de entorno para determinar parte del
formato de un texto. Un entorno tiene un nombre (cadena de caracteres alfanuméricos) y una
funcionalidad. Cada entorno comienza con “\begin{xxxx}” donde “xxxx” corresponde al nombre
del entorno en cuestión, y finaliza con “\end{xxxx}”. Un entorno puede comenzar y/o terminar en
cualquier parte de una línea. Es imprescindible que los entornos aparezcan correctamente
anidados. Entre un inicio de entorno y un fin de entorno puede aparecer cualquier texto.
Escribir un programa que verifique el correcto anidamiento de entornos en un archivo de LaTeX.
El nombre del archivo se debe recibir por línea de comandos, y el programa debe imprimir por
salida estándar si los entornos están correctamente anidados o no.
Ejemplo: en el siguiente archivo todos los entornos se encuentran correctamente anidados.
    
    ```
    \begin{document}
    Hola mundo
    \begin{center}
    Esta es una prueba de \LaTeX
    \begin{ttfamily} Esto es \emph{otra} prueba
    \end{ttfamily}
    \begin{rmfamily}
    Hola que tal
    \end{rmfamily} \end{center}
    \end{document}
    ```
    
    Los siguientes archivos son inválidos. En el primer caso porque el fin del entorno rmfamily
    aparece después del fin del entorno center y además el fin del entorno document está mal escrito.
    En el segundo caso no aparece el fin del entorno center y falta el fin del entorno document.
        
    ```
    \begin{document}
    Hola mundo
    \begin{center} Esta es una prueba de \LaTeX \begin{rmfamily}
    Hola que tal
    \end{center} \end{rmfamily}
    \theend{document}
    ```
    ```
    \begin{document}
    Hola mundo
    \begin{center}
    Esta es una prueba de \LaTeX
    \begin{rmfamily}
    Hola que tal \end{rmfamily} 
    \end{centerx}
    ```