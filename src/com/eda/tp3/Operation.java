package com.eda.tp3;

public interface Operation {
    public int apply();
}
