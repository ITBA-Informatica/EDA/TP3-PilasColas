package com.eda.tp3;

public class LinkedList<E> {
    public static class Node<E> {
        public Node prev;
        public Node next;
        public E value;
        public Node(Node prev,E value,Node next) {
            this.prev = prev;
            this.next = next;
            this.value = value;
        }

    }

    private int size = 0;
    public Node<E> head = new Node<>(null, null,null);
    public E value;
    public void add(E element) {
        if (isEmpty()) {
            Node<E> newNode = new Node<>(head,element,head);
            head.next = newNode;
            head.prev = newNode;
            size++;
        }
        addRec(head.next, element);
    }

    private void addRec(Node n, E element) {
//        Todo
    }

    public boolean isEmpty() {
        return head.next == null;
    }
}
