package com.eda.tp3;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class StackedQueue<E>{
    private Deque<E> queue = new LinkedList<>();
    private Deque<E> auxQueue = new LinkedList<>();



    public E poll() {
        return queue.pop();
    }

    public void offer(E element) {
        while(!queue.isEmpty()) {
            auxQueue.push(queue.pop());
        }
        auxQueue.push(element);
        while(!auxQueue.isEmpty()) {
            queue.push(auxQueue.pop());
        }
    }


}
