package com.eda.tp3;

import java.util.*;
import java.util.LinkedList;

public class MarketSimulator {
    private int cajasNumber = 4;
    private List<Queue<Cliente>> cajas = new LinkedList<>();
    private int currentTime;
    private int simulationTime = 10000;
    private double arrivalProbablity = 0.8;
    private int minWaitingTime = 0;
    private int maxWaitingTime = 0;
    private int totalWaitTime = 0;
    private int processedClientes = 0;
    private int totalIdleTime = 0;
    private int totalSize;

    private class Cliente {
        private Queue<Cliente> caja;
        private int minServiceTime = 4;
        private int maxServiceTime = 6;
        public int remainingTime;
        public int queuedTime;
        public int processDuration;
        public Cliente(int startingTime) {
            this.queuedTime = startingTime;
            this.remainingTime = (int) (Math.floor(Math.random() * (maxServiceTime - minServiceTime))) + minServiceTime;
            this.processDuration = this.remainingTime;
        }

        public String toString() {
            return "" + remainingTime;
        }
    }

    public MarketSimulator() {
        for (int i = 0; i < cajasNumber; i++) {
            cajas.add(new java.util.LinkedList<>());
        }

    }

    public void start() {
        while(currentTime < simulationTime) {
            simulateNewCliente(); //orden 1
            processClientes();
            System.out.println(getStatus()); // orden n
            currentTime++;
        }
        System.out.println("---- Simulacion Terminada ----");
        System.out.println("Tiempo minimo de espera: " + minWaitingTime + " (duh)");
        System.out.println("Tiempo maximo de espera: " + maxWaitingTime);
        System.out.println("Tiempo promedio de espera: " + totalWaitTime * 1.0/ processedClientes);
        System.out.println("Longitud promedio de las colas: " + totalSize * 1.0 / cajasNumber / simulationTime);
        System.out.println("Tiempo ocioso promedio de los cajeros (por unidad de tiempo) : " + (totalIdleTime * 1.0/ cajasNumber / simulationTime));
    }

    private String getStatus() {
        Iterator<Queue<Cliente>> cajasIterator = cajas.iterator();
        StringBuilder s = new StringBuilder("[");
        while(cajasIterator.hasNext()) {
            if (s.length() != 1) {
                s.append(", ");
            }
            s.append(cajasIterator.next().size());
        }
        s.append("]");
        return s.toString();
    }
    private void processClientes() {
        Iterator<Queue<Cliente>> cajasIterator = cajas.iterator();
        while(cajasIterator.hasNext()) {
            Queue<Cliente> caja = cajasIterator.next();
            totalSize += caja.size();
            if (!caja.isEmpty()) {
                Cliente nextCliente = caja.peek();
                nextCliente.remainingTime -= 1;
                if (nextCliente.remainingTime == 0) {
                    processedClientes++;
                    caja.poll();
                    int waitTime = (currentTime + 1 - nextCliente.queuedTime - nextCliente.processDuration);
                    totalWaitTime += waitTime;
                    if (waitTime > maxWaitingTime) {
                        maxWaitingTime = waitTime;
                    }
                    if (waitTime < minWaitingTime) {
                        minWaitingTime = waitTime;
                    }
                }
            } else {
                totalIdleTime +=1;
            }
        }
    }

    private void simulateNewCliente() {
        if (Math.random() < arrivalProbablity) {
            Cliente newCliente = new Cliente(currentTime);
//            Si bien esta era la "gracia" del ejercicio, lo use con el stram de java
//            Hay una alternativa a hacerlo asi que es la siguiente: Como ya estoy iterando por las n cajas cuando
//            Proceso a los clientes, puedo usar esa misma iteracion para guardarme la caja con n minimo tambien
//            Si bien eso es mas eficiente, queda mas feo en el codigo, y ademas no altera el orden de complejidad
//            (sigue siendo orden n)
            Queue<Cliente> cajaMinima = cajas.stream().min(Comparator.comparing(Queue::size)).orElseThrow(NoSuchElementException::new);
            cajaMinima.offer(newCliente);
        }
    }
}
