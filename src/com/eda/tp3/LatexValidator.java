package com.eda.tp3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Scanner;

public class LatexValidator {
    public boolean validate(String path) {
        Scanner input;
        try {
            File file = new File(path);
            input = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            return false;
        }

        Deque<String> stack = new LinkedList<>();
        while(input.hasNext()) {
            String nextSentence = input.next();
            if (nextSentence.length() > 9 &&  nextSentence.substring(0,7).equals("\\begin{") && nextSentence.charAt(nextSentence.length() - 1) == '}') {
                stack.push(nextSentence.substring(7, nextSentence.length() - 1));
            }
            else if (nextSentence.length() > 7 &&  nextSentence.substring(0,5).equals("\\end{") && nextSentence.charAt(nextSentence.length() - 1) == '}') {
                String toPopContext = nextSentence.substring(5, nextSentence.length() - 1);
                if (!stack.pop().equals(toPopContext)) {
                    return false;
                }
            }
        }
        return (stack.size() == 0);
    }
}
