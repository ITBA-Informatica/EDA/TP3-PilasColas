package com.eda.tp3;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;

public class PhoneCombinations {

//    El Ej 15 es muy parecido al 12 o al 13, pero con la diferencia de que cada vez que encuentro
//    una alteracion numerica, en lugar de imprimirla, busco las iteraciones nuevamente de los numeros que la conforman

    public static long printCombinationsTest(int n) {
        long startTime = System.currentTimeMillis();
        printCombinations(n);
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        return duration;
    }

    public static void printCombinations(int n) {
        char[] chars = (String.valueOf(n)).toCharArray();
        int[] numbers = new int[chars.length];
        for (int i = 0; i < chars.length; i++) {
            numbers[i] = (int) (chars[i] - 48);
        }

        Deque<StringBuilder> stack1 = new LinkedList<>();
        Deque<StringBuilder> stack2 = new LinkedList<>();
        for (int i = 0; i < numbers.length; i++) {
            char[] numberCombinations = possibleChars(numbers[i]);
//            Primer caso, agregoo todas las combinaciones al stack 1
            if (i == 0) {
                for (int j = 0; j < numberCombinations.length; j++) {
                    stack1.push(new StringBuilder(String.valueOf(numberCombinations[j])));
                }
//            Demas casos, por cada elemento en el stack lleno (creo que esto se soluciona con una cola, y preguntando por el tamaño)
//            Paso todas las posibnles combinaciones al stack vacio
            } else {
                if (!stack1.isEmpty()) {
                    while (!stack1.isEmpty()) {
                        StringBuilder s = stack1.pop();
                        for (int j = 0; j < numberCombinations.length; j++) {
                            StringBuilder newS = new StringBuilder(s.toString());
                            newS.append(numberCombinations[j]);
                            stack2.push(newS);
                        }
                    }
                } else if(!stack2.isEmpty()) {
                    while (!stack2.isEmpty()) {
                        StringBuilder s = stack2.pop();
                        for (int j = 0; j <numberCombinations.length; j++) {
                            StringBuilder newS = new StringBuilder(s.toString());
                            newS.append(numberCombinations[j]);
                            stack1.push(newS);
                        }
                    }
                }
            }
        }
        if (!stack1.isEmpty()) {
            System.out.println(stack1);
        } else {
            System.out.println(stack2);
        }
    }

    public static char[] possibleChars(int n) {
        int base = 97 + 3 * (n-2);
        if (n < 7){
            char[] chars = {(char) (base), (char) (base+1), (char) (base+2)};
            return chars;
        }
        if (n == 7){
            char[] chars = {(char) (base), (char) (base+1), (char) (base+2), (char) (base+3)};
            return chars;
        }
        if (n == 9){
            base += 1;
            char[] chars = {(char) (base), (char) (base+1), (char) (base+2), (char) (base+3)};
            return chars;
        }
        base += 1;
        char[] chars = {(char) (base), (char) (base+1), (char) (base+2)};
        return chars;
    }

}
