package com.eda.tp3;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;

public class PostFixProcessor {
    public static double process (String exp){
        Deque<Double> stack = new LinkedList<>();

        CharIterator it = new CharIterator(exp);
        double currentNum = 0;
        while(it.hasNext()) {
            Character c = it.next();
            BinaryOperator op = BinaryOperator.fromChar(c);
            if (op == null) {
                if (c == ' ') {
                    if (currentNum != 0) {
                        stack.push(currentNum);
                        currentNum = 0;
                    }
                } else {
                    Double num = Double.valueOf(String.valueOf(c));
                    currentNum = currentNum * 10 + num;
                }
            } else {
                if (currentNum != 0) {
                    stack.push(currentNum);
                    currentNum = 0;
                }
                if (stack.size() < 2) {
                    throw new RuntimeException("Invalid operation");
                }
                Double term2 = stack.pop();
                Double term1 = stack.pop();
                stack.push(op.applyAsDouble(term1,term2));
            }

        }
        return stack.pop();
    }
}
