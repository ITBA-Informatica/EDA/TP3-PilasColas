package com.eda.tp3;

import java.util.Iterator;

class CharIterator implements Iterator<Character> {

    private String str;
    private int pos = 0;

    public CharIterator(String str) {
        this.str = str;
    }

    public boolean hasNext() {
        return pos < str.length();
    }

    public Character next() {
        return str.charAt(pos++);
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }
}