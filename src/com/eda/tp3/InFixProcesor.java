package com.eda.tp3;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;

public class InFixProcesor {

    public static double process(String exp) {
        CharIterator it = new CharIterator(exp);
        StringBuffer processedString = new StringBuffer();

        processRec(it, processedString);
        return PostFixProcessor.process(processedString.toString());
    }

    public static CharIterator processRec(CharIterator it, StringBuffer exp) {
        Deque<BinaryOperator> stack = new LinkedList<>();

        double currentNum = 0;
        while(it.hasNext()) {
            Character c = it.next();
            BinaryOperator op = BinaryOperator.fromChar(c);
            if (c == '(') {
                if (currentNum != 0) {
                    throw new IllegalArgumentException();
                }
                processRec(it, exp);
            }
            else if (c == ')') {
                if (currentNum != 0) {
                    exp.append(' ').append((int) currentNum);
                }
                while (!stack.isEmpty()) {

                    BinaryOperator toAddOp = stack.pop();
                    exp.append(" ").append(toAddOp.symbol);
                }
                return it;
            }
            else if (c == ' ') {
                if (currentNum != 0) {
                    exp.append(' ').append((int) currentNum);
                    currentNum = 0;
                }
            }
            else if (op == null) { // me llego un numero
                Double num = Double.valueOf(String.valueOf(c));
                currentNum = currentNum * 10 + num;
            } else { // me llego un operador
                if (currentNum != 0) {
                    exp.append(' ').append((int) currentNum);
                    currentNum = 0;
                }
                while (!stack.isEmpty() && stack.peek().priority >= op.priority) {
                    BinaryOperator toAddOp = stack.pop();
                    exp.append(" ").append(toAddOp.symbol);
                }
                stack.push(op);
            }
        }
        if (currentNum != 0) {
            exp.append(' ').append((int) currentNum);
        }
        while (!stack.isEmpty()) {

            BinaryOperator toAddOp = stack.pop();
            exp.append(" ").append(toAddOp.symbol);
        }
        return it;
    }
}
