package com.eda.tp3;

import java.util.LinkedList;
import java.util.Queue;

public class PrintSimulator {
    double arrivalProbablity = 0.1;
    private int idCounter = 0;
    private int currentTime;
    private int simulationTime = 10000;
    private Queue<PrintJob> jobs = new LinkedList<>();
    private int jobsInQueue;
    private int totalWaitTime;

    private int processedJobs;

    private class PrintJob {
        private int minServiceTime = 5;
        private int maxServiceTime = 10;
        public int remainingTime;
        public int id;
        public int queuedTime;
        public int processDuration;
        public PrintJob(int startingTime) {
            this.queuedTime = startingTime;
            this.remainingTime = this.randomizeTime();
            this.processDuration = this.remainingTime;
        }

        private int randomizeTime() {
            return  (int) (Math.floor(Math.random() * (maxServiceTime - minServiceTime))) + minServiceTime;
        }

        public String toString() {
            return "[" + id +"]" + " (" + remainingTime + ")";
        }
    }

    public void start() {
        while(currentTime < simulationTime) {
            simulateNewJob(); //orden 1
            jobsInQueue += jobs.size();
            processNextJob(); // orden 1

            currentTime++;
        }

        System.out.println("Trabajos atendidos: " + processedJobs);
        System.out.println("Promedio de trabajos en cola: " + (jobsInQueue * 1.0 / simulationTime));
        System.out.println("Promeido de tiempo en cola: " + (totalWaitTime * 1.0 / processedJobs));

    }

    private void simulateNewJob() {
        if (Math.random() < arrivalProbablity) {
            PrintJob newJob = new PrintJob(currentTime);
            newJob.id = idCounter++;
            jobs.offer(newJob);
        }
    }

    private void processNextJob() {
        if (!jobs.isEmpty()) {
            PrintJob nextJob = jobs.peek();
            nextJob.remainingTime--;
            if (nextJob.remainingTime == 0) {
                jobs.poll();
                totalWaitTime += (currentTime - nextJob.queuedTime - nextJob.processDuration);
                processedJobs++;
            }
        }
    }


}
