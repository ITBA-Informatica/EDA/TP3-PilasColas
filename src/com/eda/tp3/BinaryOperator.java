package com.eda.tp3;

import java.util.function.DoubleBinaryOperator;

public enum BinaryOperator implements DoubleBinaryOperator {

    PLUS("+", (a, b) -> a + b, 3),
    MINUS("-", (a, b) -> a - b, 3),
    MUL("*", (a, b) -> a * b, 4),
    DIV("/", (a, b) -> a / b, 4);

    private final DoubleBinaryOperator f;
    public  String symbol;
    public int priority;
    BinaryOperator(String c, DoubleBinaryOperator f, int p) {
        this.symbol = c;
        this.f = f;
        this.priority = p;
    }

    @Override
    public double applyAsDouble(double a, double b) {
        return f.applyAsDouble(a, b);
    }

    public static BinaryOperator fromChar(char c) {
        switch (c) {
            case '+':
                return BinaryOperator.PLUS;
            case '-':
                return BinaryOperator.MINUS;
            case '*':
                return BinaryOperator.MUL;
            case '/':
                return BinaryOperator.DIV;
        }
        return null;
    }



    public String toString() {
        return (this.symbol);
    }
}