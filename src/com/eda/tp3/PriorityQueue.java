package com.eda.tp3;

import java.util.LinkedList;
import java.util.Queue;

public class PriorityQueue<E> {

    public  static  class Node<E> {
        public Node<E> prev;
        public Node<E> next;
        public Queue<E> list;
        public int priority;

        public Node(Node<E> prev, E element, Node<E> next, int priority) {
            this.prev = prev;
            this.next = next;
            this.priority = priority;
            this.list = new LinkedList<>();
            this.list.offer(element);
        }

        public String toString() {
            return "";
        }

        public void add(E element) {
            list.offer(element);
        }

        public boolean isEmpty() {
            return list.isEmpty();
        }

        public E poll() {
            return list.poll();
        }

    }

    private int size = 0;
    public Node<E> head = new Node<E>(null, null, null, 1);
    public E value;

    public void enqueue(E element, int priority) {
        if (isEmpty()) {
            Node<E> newNode = new Node<>(head,element, head, priority);
            head.next = newNode;
            head.prev = newNode;
            size++;
        } else {
            addRec(head, element, priority);
        }
    }

    public E dequeue() {
        if (isEmpty()) return null;
        E resp = head.next.poll();
        if (head.next.isEmpty()) {
            if (head.next == head.prev) {
                head.next = null;
                head.prev = null;
            } else {
                head.next.next.prev = head;
                head.next = head.next.next;
            }
        }
        return resp;
    }

    private void addRec(Node<E> n, E element, int priority) {
        if (n.next == head) {
            Node<E> newNode = new Node<>(n, element,head, priority);
            n.next = newNode;
            head.prev = newNode;
        } else if (n.next.priority == priority) {
            n.next.add(element);
        } else if(n.next.priority > priority) {
            Node<E> newNode = new Node<>(n, element,n.next, priority);
            n.next.prev= newNode;
            n.next = newNode;
        } else {
            addRec(n.next,element,priority);
        }
    }

    public boolean isEmpty() {
        return head.next == null;
    }

    public String toString() {
        StringBuilder s = new StringBuilder();
        if (isEmpty()) return "[]";
        s.append("[");
        toStringRec(s,head.next);
        return s.toString();
    }

    private void toStringRec(StringBuilder s, Node<E> n) {
        if (n == head) {
            s.append("]");
            return;
        }
        if (n != head.next) {
            s.append(", ");
        }
        s.append(n.toString());
        toStringRec(s,n.next);
    }
}
